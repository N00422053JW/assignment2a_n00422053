﻿<%@ Page Language="C#" MasterPageFile="~/sitemaster.master" AutoEventWireup="true" %>

<asp:Content runat="server" ContentPlaceHolderID="title"><h2>ASP.NET</h2></asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="body"><p>ASP.NET is an open-source server-side web application framework 
designed for web development to produce dynamic web pages. It was developed by Microsoft to allow programmers to build dynamic 
web sites, web applications and web services.</p></asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="subtitle">
<ul>
    <li>Validation controls</li>
    <li>Databases</li>
    <li>Forms</li>
    <li>WebGrid</li>
</ul>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="detail">
<a href="https://www.asp.net/freecourses" title="ASP.NET tutorial">Tutorial</a></asp:Content>