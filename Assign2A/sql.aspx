﻿<%@ Page Language="C#" MasterPageFile="~/sitemaster.master" AutoEventWireup="true" %>

<asp:Content runat="server" ContentPlaceHolderID="title"><h2>SQL</h2></asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="body"><p>SQL (Structured Query Language) is a database management language 
for relational databases. SQL itself is not a programming language, but its standard allows creating procedural extensions for 
it, which extend it to functionality of a mature programming language.</p></asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="subtitle">
<ul>
    <li>Select</li>
    <li>Where</li>
    <li>And,Or,Not</li>
    <li>Between</li>
    <li>Join</li>
    <li>Where</li>
    <li>Insert into</li>
    <li>Min, Max</li>
    <li>Count, Avg, Sum</li>
    <li>Group by</li>
    <li>Having</li>
</ul>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="detail">
<a href="https://www.w3schools.com/sql/default.asp" title="SQL tutorial">Tutorial</a></asp:Content>