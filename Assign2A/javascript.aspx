﻿<%@ Page Language="C#" MasterPageFile="~/sitemaster.master" AutoEventWireup="true" %>

<asp:Content runat="server" ContentPlaceHolderID="title"><h2>Javascript</h2></asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="body"><p>JavaScript is the language of the web. You can use it to add 
dynamic behavior, store information, and handle requests and responses on a website. The concepts covered in these courses 
lay the foundation for adding this behavior.</p></asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="subtitle">
<ul>
    <li>Comments</li>
    <li>Output</li>
    <li>Statements</li>
    <li>Variables</li>
    <li>Operators</li>
    <li>Loop</li>
    <li>Array</li>
    <li>Functions</li>
    <li>Objects</li>
</ul>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="detail">
<a href="https://www.w3schools.com/js/default.asp" title="Javascript tutorial">Tutorial</a></asp:Content>
 
