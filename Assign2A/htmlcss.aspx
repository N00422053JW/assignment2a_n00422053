﻿<%@ Page Language="C#" MasterPageFile="~/sitemaster.master" AutoEventWireup="true" %>

<asp:Content runat="server" ContentPlaceHolderID="title"><h2>HTML/CSS</h2></asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="body">CSS (Cascading Style Sheets) is used to style and lay out web pages — 
for example, to alter the font, colour, size and spacing of your content, split it into multiple columns, or add animations and 
other decorative features. This module gets you started on the path to CSS mastery with the basics of how it works, 
including selectors and properties, writing CSS rules, applying CSS to HTML, how to specify length, colour, and other units 
in CSS, cascade and inheritance, and debugging CSS.</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="subtitle">
<ul>
    <li>Margins/Padding</li>
    <li>Colors</li>
    <li>Fonts</li>
    <li>List</li>
    <li>Float</li>
    <li>Overflow</li>
    <li>Position</li>
    <li>Forms</li>
    <li>Inline-block</li>
    <li>Align</li>
</ul>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="detail">
<a href="https://www.w3schools.com/css/default.asp" title="CSS tutorial">Tutorial</a></asp:Content>